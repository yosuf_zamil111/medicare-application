package com.stsbd.towhid.medicineapplication.activity.adapter;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.stsbd.towhid.medicineapplication.R;
import com.stsbd.towhid.medicineapplication.databinding.ItemSingleStringBinding;
import com.stsbd.towhid.medicineapplication.model.DrugInfo;

import java.util.List;

/**
 * Created by towhid on 05-Feb-17.
 */
public class SingleStringAdapter extends RecyclerView.Adapter<SingleStringAdapter.DrugViewHolder> {

    private List<DrugInfo> drugList;

    public SingleStringAdapter(List<DrugInfo> drugs) {
        this.drugList = drugs;
    }

    @Override
    public DrugViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemSingleStringBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.item_single_string,
                parent,
                false);
        return new DrugViewHolder(binding.getRoot());
    }

    @Override
    public void onBindViewHolder(DrugViewHolder holder, int position) {
        DrugInfo aDrug = drugList.get(position);
        ItemSingleStringBinding binding = DataBindingUtil.bind(holder.itemView);
        binding.setModel(aDrug);
        binding.executePendingBindings();

        // Here you apply the animation when the view is bound
//        setFadeAnimation(holder.itemView);
    }

    @Override
    public int getItemCount() {
        return drugList.size();
    }

    public void refreshData(List<DrugInfo> drugs) {
        drugList.clear();
        drugList.addAll(drugs);
        notifyDataSetChanged();
    }

    public class DrugViewHolder extends RecyclerView.ViewHolder {
        public DrugViewHolder(View itemView) {
            super(itemView);
        }
    }

}
