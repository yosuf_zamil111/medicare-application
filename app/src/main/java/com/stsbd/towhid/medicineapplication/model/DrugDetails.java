package com.stsbd.towhid.medicineapplication.model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

/**
 * Created by ehsan on 01-Feb-17.
 */

@Table(name = "drug_info", id = "_id")
public class DrugDetails extends Model {
    @Column
    public String brand_id;
    @Column
    public String generic_id;
    @Column
    public String company_id;
    @Column
    public String brand_name;
    @Column
    public String form;
    @Column
    public String strength;
    @Column
    public String price;
    @Column
    public String packsize;
    @Column
    public String updated_at;
    @Column
    public String created_at;

    public DrugDetails() {
        brand_id = "demo";
        generic_id = "demo";
        company_id = "demo";
        brand_name = "demo";
        form = "demo";
        strength = "demo";
        price = "demo";
        packsize = "demo";
        updated_at = "demo";
        created_at = "demo";
    }

    public DrugDetails(String brand_id, String generic_id, String company_id, String brand_name, String form, String strength, String price, String packsize, String updated_at, String created_at) {
        this.brand_id = brand_id;
        this.generic_id = generic_id;
        this.company_id = company_id;
        this.brand_name = brand_name;
        this.form = form;
        this.strength = strength;
        this.price = price;
        this.packsize = packsize;
        this.updated_at = updated_at;
        this.created_at = created_at;
    }

    @Override
    public String toString() {
        return "DrugDetails{" +
                "brand_id='" + brand_id + '\'' +
                ", generic_id='" + generic_id + '\'' +
                ", company_id='" + company_id + '\'' +
                ", brand_name='" + brand_name + '\'' +
                ", form='" + form + '\'' +
                ", strength='" + strength + '\'' +
                ", price='" + price + '\'' +
                ", packsize='" + packsize + '\'' +
                ", updated_at='" + updated_at + '\'' +
                ", created_at='" + created_at + '\'' +
                '}';
    }
}
