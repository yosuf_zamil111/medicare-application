package com.stsbd.towhid.medicineapplication;

import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.activeandroid.query.Select;
import com.stsbd.towhid.medicineapplication.activity.AllDrugsActivity;
import com.stsbd.towhid.medicineapplication.activity.GenericActivity;
import com.stsbd.towhid.medicineapplication.activity.Search_Activity;
import com.stsbd.towhid.medicineapplication.model.DrugDetails;
import com.stsbd.towhid.medicineapplication.model.DrugInfo;
import com.stsbd.towhid.medicineapplication.utils.mCons;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;
import java.util.List;


public class MainActivity extends AppCompatActivity {

    private TextView viewData;
    private mApp app;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        viewData = (TextView) findViewById(R.id.viewData);
        app = new mApp();
//        startActivity(new Intent(this, AllDrugsActivity.class));
    }


    public void onClicked_search(View v) {
        startActivity(new Intent(this, Search_Activity.class));
    }

    public void onClicked_all_drugs(View view) {
        startActivity(new Intent(this, AllDrugsActivity.class));
    }

    public void onClicked_drug_by_generic(View v) {
        startActivity(new Intent(this, GenericActivity.class));
    }

    public void onClicked_drug_by_class(View v) {
//        startActivity(new Intent(this,. class));
    }

    public void onClicked_drug_by_indication(View v) {
//        startActivity(new Intent(this,. class));
    }

    public void onClicked_favourite_drugs(View v) {
//        startActivity(new Intent(this,. class));
    }

    public void onClicked_about_us(View v) {
        new AlertDialog.Builder(this)
                .setTitle("About us")
                .setMessage("Here is the details about the app..")
                .setPositiveButton("Ok", null)
                .show();
    }

    public void onClicked_add_drug(View v) {
//        startActivity(new Intent(this,. class));
    }


    // testing purpose.................

    public void onClick_read(View view) {
        DrugInfo drugData = mApp.getRandom();
        List<DrugInfo> list = new Select(new String[]{mCons.col_generic_name})
                .from(DrugInfo.class)
//                .where()
                .execute();
        StringBuilder sb = new StringBuilder("new");
        if (list.size() > 0) for (DrugInfo d : list) sb.append(d.generic_name).append("\n\n");


        int size = new Select().from(DrugInfo.class).execute().size();
        Toast.makeText(this, "" + size, Toast.LENGTH_SHORT).show();
                /*new Select()
                .from(DrugInfo.class)
//                .where("_id = 2")
                .orderBy("RANDOM()")
//                .orderBy("generic_name ASC")
//                .orderBy("null")
                .executeSingle();
//                .execute();*/
        if (drugData == null)
            Toast.makeText(this, "null", Toast.LENGTH_SHORT).show();
        else viewData.setText(drugData.toString());
    }

    public void onClick_copy(View view) {
        copy2Storage();
    }


    public void copy2Storage() {

        try {
            File sd = Environment.getExternalStorageDirectory();
            File data = Environment.getDataDirectory();

            if (sd.canWrite()) {
                String currentDBPath = "/data/data/" + getPackageName() + "/databases/medicine_database.db";
                String backupDBPath = "extracted.db";
                File currentDB = new File(currentDBPath);
                File backupDB = new File(sd, backupDBPath);

                if (currentDB.exists()) {
                    FileChannel src = new FileInputStream(currentDB).getChannel();
                    FileChannel dst = new FileOutputStream(backupDB).getChannel();
                    dst.transferFrom(src, 0, src.size());
                    src.close();
                    dst.close();
                    Toast.makeText(this, "Successfully copied!", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(this, "File doesn't exists!", Toast.LENGTH_SHORT).show();
                }
            } else Toast.makeText(this, "No write permission!", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            Toast.makeText(this, "Exception occurred!", Toast.LENGTH_SHORT).show();
        }
    }

    public void onClick_create(View view) {
        new DrugInfo().save();
        new DrugDetails().save();
    }

}
