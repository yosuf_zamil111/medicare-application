package com.stsbd.towhid.medicineapplication.model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

/**
 * Created by towhid on 31-Jan-17.
 */

@Table(name = "drug_info", id = "_id")
public class DrugInfo extends Model {
    @Column
    public String administration;
    @Column
    public String adult_dose;
    @Column
    public String child_dose;
    @Column
    public String contra_indication;
    @Column
    public String created_at;
    @Column
    public String dose;
    @Column
    public String generic_id;
    @Column
    public String generic_name;
    @Column
    public String indication;
    @Column
    public String interaction;
    @Column
    public String mode_of_action;
    @Column
    public String precaution;
    @Column
    public String pregnancy_category_id;
    @Column
    public String pregnancy_category_note;
    @Column
    public String renal_dose;
    @Column
    public String side_effect;
    @Column
    public String updated_at;

    public DrugInfo() {
        administration = "demo";
        adult_dose = "demo";
        child_dose = "demo";
        contra_indication = "demo";
        created_at = "demo";
        dose = "demo";
        generic_id = "demo";
        generic_name = "demo";
        indication = "demo";
        interaction = "demo";
        mode_of_action = "demo";
        precaution = "demo";
        pregnancy_category_id = "demo";
        pregnancy_category_note = "demo";
        renal_dose = "demo";
        side_effect = "demo";
        updated_at = "demo";
    }

    public DrugInfo(String administration, String adult_dose, String child_dose, String contra_indication, String created_at, String dose, String generic_id, String generic_name, String indication, String interaction, String mode_of_action, String precaution, String pregnancy_category_id, String pregnancy_category_note, String renal_dose, String side_effect, String updated_at) {
        this.administration = administration;
        this.adult_dose = adult_dose;
        this.child_dose = child_dose;
        this.contra_indication = contra_indication;
        this.created_at = created_at;
        this.dose = dose;
        this.generic_id = generic_id;
        this.generic_name = generic_name;
        this.indication = indication;
        this.interaction = interaction;
        this.mode_of_action = mode_of_action;
        this.precaution = precaution;
        this.pregnancy_category_id = pregnancy_category_id;
        this.pregnancy_category_note = pregnancy_category_note;
        this.renal_dose = renal_dose;
        this.side_effect = side_effect;
        this.updated_at = updated_at;
    }


    @Override
    public String toString() {
        return "DrugInfo{" +
                ", administration='" + administration + '\'' +
                ", adult_dose='" + adult_dose + '\'' +
                ", child_dose='" + child_dose + '\'' +
                ", contra_indication='" + contra_indication + '\'' +
                ", created_at='" + created_at + '\'' +
                ", dose='" + dose + '\'' +
                ", generic_id='" + generic_id + '\'' +
                ", generic_name='" + generic_name + '\'' +
                ", indication='" + indication + '\'' +
                ", interaction='" + interaction + '\'' +
                ", mode_of_action='" + mode_of_action + '\'' +
                ", precaution='" + precaution + '\'' +
                ", pregnancy_category_id='" + pregnancy_category_id + '\'' +
                ", pregnancy_category_note='" + pregnancy_category_note + '\'' +
                ", renal_dose='" + renal_dose + '\'' +
                ", side_effect='" + side_effect + '\'' +
                ", updated_at='" + updated_at + '\'' +
                '}';
    }

    public String getGeneric_name() {
        return generic_name.contains("+") ? generic_name.substring(0, generic_name.indexOf("+") - 1) : generic_name;
    }
}
