package com.stsbd.towhid.medicineapplication.activity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.activeandroid.query.Select;
import com.stsbd.towhid.medicineapplication.R;
import com.stsbd.towhid.medicineapplication.activity.adapter.DrugAdapter;
import com.stsbd.towhid.medicineapplication.model.DrugInfo;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;
import java.util.List;

public class AllDrugsActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {
    ListView listView;
    RecyclerView recycler;
    AVLoadingIndicatorView avi;
    private List<DrugInfo> drugs;
    private DrugAdapter mAdapter;
    private String[] alphabets = new String[]{"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};
    private AsyncTask<String, String, Boolean> mTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_drugs);
        listView = (ListView) findViewById(R.id.listView);
        recycler = (RecyclerView) findViewById(R.id.recyclerView);

        recycler.setHasFixedSize(true);
        recycler.setLayoutManager(new LinearLayoutManager(this));
        avi = (AVLoadingIndicatorView) findViewById(R.id.avi);

        avi.hide(); // -----------------------

        drugs = new ArrayList<>();
        mAdapter = new DrugAdapter(drugs);
        recycler.setAdapter(mAdapter);
        mTask = new AsyncDataLoader();
        mTask.execute("a");

        listView.setAdapter(new ArrayAdapter<>(this, R.layout.item_single_character, android.R.id.text1, alphabets));
        listView.setOnItemClickListener(this);
    }

    String tempCharHolder = "";

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        String alp = alphabets[position].toLowerCase();
        if (!tempCharHolder.equals(alp)) {
            if (mTask.getStatus() == AsyncTask.Status.RUNNING || mTask.getStatus() == AsyncTask.Status.PENDING)
                mTask.cancel(true);
            new AsyncDataLoader().execute(alp);
        }
    }


    private class AsyncDataLoader extends AsyncTask<String, String, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            avi.show();
        }

        @Override
        protected Boolean doInBackground(String... params) {

            //this method will be running on background thread so don't update UI frome here
            //do your long running http tasks here,you dont want to pass argument and u can access the parent class' variable url over here
            drugs = new Select().from(DrugInfo.class)
                    .where("generic_name like '" + params[0] + "%'").execute();
            return drugs.size() > 0;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
//            Toast.makeText(AllDrugsActivity.this, "in adapter" + mAdapter.getItemCount() + "\nin list " + drugs.size(), Toast.LENGTH_SHORT).show();
            if (result) mAdapter.refreshData(drugs);
            else Toast.makeText(AllDrugsActivity.this, "failed!", Toast.LENGTH_SHORT).show();

            //this method will be running on UI thread
            avi.hide();
        }

    }
}
