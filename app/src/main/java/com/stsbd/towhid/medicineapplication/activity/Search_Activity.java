package com.stsbd.towhid.medicineapplication.activity;

import android.app.SearchManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.activeandroid.query.Select;
import com.stsbd.towhid.medicineapplication.R;
import com.stsbd.towhid.medicineapplication.activity.adapter.DrugAdapter;
import com.stsbd.towhid.medicineapplication.model.DrugInfo;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by ehsan on 05-Feb-17.
 */

public class Search_Activity extends AppCompatActivity implements SearchView.OnQueryTextListener {
    private AVLoadingIndicatorView avi;
    private DrugAdapter mAdapter;
    private List<DrugInfo> drugs;
    private RecyclerView recycler;
    private TextView backHint;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        avi = (AVLoadingIndicatorView) findViewById(R.id.avi);
        recycler = (RecyclerView) findViewById(R.id.recyclerView);
        backHint = (TextView) findViewById(R.id.searchMedicineBack);

        recycler.setHasFixedSize(true);
        recycler.setLayoutManager(new LinearLayoutManager(this));

        drugs = new ArrayList<>();
        mAdapter = new DrugAdapter(drugs);
        recycler.setAdapter(mAdapter);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_main, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchManager searchManager = (SearchManager) getSystemService(SEARCH_SERVICE);
        SearchView searchView = null;
        if (searchItem != null) {
            searchView = (SearchView) searchItem.getActionView();
        }
        if (searchView != null) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
            searchView.setOnQueryTextListener(this);
        }

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onQueryTextChange(String q) {
        // Here is where we are going to implement the filter logic
        if (avi.isShown()) return false;
        new AsyncDataLoader().execute(q);
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    private class AsyncDataLoader extends AsyncTask<String, String, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            avi.show();
        }

        @Override
        protected Boolean doInBackground(String... params) {

            //this method will be running on background thread so don't update UI frome here
            //do your long running http tasks here,you dont want to pass argument and u can access the parent class' variable url over here
            String str = params[0].equals("") ? "a" : params[0];
            drugs = new Select().from(DrugInfo.class)
                    .where("generic_name like '" + str + "%'").execute();
            return drugs.size() > 0;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
//            Toast.makeText(AllDrugsActivity.this, "in adapter" + mAdapter.getItemCount() + "\nin list " + drugs.size(), Toast.LENGTH_SHORT).show();
            if (result) {
                mAdapter.refreshData(drugs);
                backHint.setVisibility(View.GONE);
            } else {
                mAdapter.refreshData(new ArrayList<DrugInfo>());
                backHint.setText("No match found!");
                backHint.setVisibility(View.VISIBLE);
            }
//            else Toast.makeText(Search_Activity.this, "failed!", Toast.LENGTH_SHORT).show();

            //this method will be running on UI thread
            avi.hide();
        }

    }
}
