package com.stsbd.towhid.medicineapplication.activity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.activeandroid.query.Select;
import com.stsbd.towhid.medicineapplication.R;
import com.stsbd.towhid.medicineapplication.activity.adapter.SingleStringAdapter;
import com.stsbd.towhid.medicineapplication.model.DrugInfo;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;
import java.util.List;

public class GenericActivity extends AppCompatActivity {

    private RecyclerView recycler;
    private AVLoadingIndicatorView avi;
    private List<DrugInfo> drugs;
    private SingleStringAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_generic);
        avi = (AVLoadingIndicatorView) findViewById(R.id.avi);
        recycler = (RecyclerView) findViewById(R.id.recyclerView);

        recycler.setHasFixedSize(true);
        recycler.setLayoutManager(new LinearLayoutManager(this));

        drugs = new ArrayList<>();
        mAdapter = new SingleStringAdapter(drugs);
        recycler.setAdapter(mAdapter);
        new AsyncDataLoader().execute();
    }


    private class AsyncDataLoader extends AsyncTask<Void, Void, Boolean> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            avi.show();
        }

        @Override
        protected Boolean doInBackground(Void... params) {

            //this method will be running on background thread so don't update UI frome here
            //do your long running http tasks here,you dont want to pass argument and u can access the parent class' variable url over here
            drugs = new Select().from(DrugInfo.class)
                    .orderBy("generic_name ASC").execute();
            return drugs.size() > 0;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            if (result) mAdapter.refreshData(drugs);
            else Toast.makeText(GenericActivity.this, "failed!", Toast.LENGTH_SHORT).show();

            //this method will be running on UI thread
            avi.hide();
        }

    }
}
