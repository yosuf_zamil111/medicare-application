package com.stsbd.towhid.medicineapplication;

import android.os.Environment;
import android.widget.Toast;

import com.activeandroid.app.Application;
import com.activeandroid.query.Select;
import com.stsbd.towhid.medicineapplication.model.DrugInfo;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;

/**
 * Created by towhid on 02-Feb-17.
 */

public class mApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
//        dbHelper = new DatabaseHelper(this);
//        dbHelper.onUpgrade(dbHelper.getWritableDatabase(), 1, 2);
//        db = dbHelper.getWritableDatabase();
    }

    public static DrugInfo getRandom() {
        return new Select().from(DrugInfo.class).orderBy("RANDOM()").executeSingle();
    }

    /*public SQLiteDatabase getDB() {
        return db != null ? db : dbHelper.getWritableDatabase();
    }*/
}
