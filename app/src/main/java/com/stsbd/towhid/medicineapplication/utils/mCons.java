package com.stsbd.towhid.medicineapplication.utils;

/**
 * Created by ehsan on 05-Feb-17.
 */

public class mCons {
    public static final String col_administration = "administration";
    public static final String col_adult_dose = "adult_dose";
    public static final String col_child_dose = "child_dose";
    public static final String col_contra_indication = "contra_indication";
    public static final String col_created_at = "created_at";
    public static final String col_dose = "dose";
    public static final String col_generic_id = "generic_id";
    public static final String col_generic_name = "generic_name";
    public static final String col_indication = "indication";
    public static final String col_interaction = "interaction";
    public static final String col_mode_of_action = "mode_of_action";
    public static final String col_precaution = "precaution";
    public static final String col_pregnancy_category_id = "pregnancy_category_id";
    public static final String col_pregnancy_category_note = "pregnancy_category_note";
    public static final String col_renal_dose = "renal_dose";
    public static final String col_side_effect = "side_effect";
    public static final String col_updated_at = "updated_at";
}
